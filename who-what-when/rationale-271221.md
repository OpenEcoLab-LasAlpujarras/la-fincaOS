[FarmOS](https://farmos.org) is super cool!

with **la-fincaOS** we seek to develop a growing 'local-first' suite of digital tools with practical application for a direct positive action.
    - [remedium](https://remedium.tqt.solutions) is able to offer mutual benefit testbed environments 
        - cielotierra
        - el alquimista
        - finca jassab
        - finca pedronella
        - finca tranquilo
